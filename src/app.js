import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, Image, ScrollView, ListView} from 'react-native';
import { data } from "../constants";
const {height, width} = Dimensions.get('window');
import cards from '../store/index.js';
const dataBlob = {};
const sectionIDs = [];
const rowIDs = [];
for (let i = 0; i < cards.length; i++) {
  dataBlob['s' + i] = i;
  dataBlob[`s${i}:r${i}`] = i;
  sectionIDs.push('s' + i);
  rowIDs.push(['r' + i]);
}

var styles = StyleSheet.create({
  scrollbar: {
      minHeight: 100,
      marginTop: 20
  },
  container: {
      // backgroundColor:'blue',
      // flex: 1,
      alignItems: 'center',
      marginLeft: 10,
      marginRight: 10,
  },
  card: {
    // height: 40,
    transform: [
      // { translateX: - Dimensions.get('window').width * 0.24 },
      // { perspective: 800 },
    ]
  },
  child: {
    // backgroundColor: 'blue',
    transform: [
      // { translateX: - Dimensions.get('window').width * 0.24 },
      // { perspective: 800 },
      // { rotateX: '-40deg'},
    ],
  }
});

class Card extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Image source={require("../img/rocket.png")} resizeMode='center' style={StyleSheet.flatten([styles.child, {width}])} />
      </View>
    );
  }
  scroll(){

  }
}

class Cards extends Component {
    constructor(){
        super();
    }
    componentDidMount(){

    }
    zIndex(i){
        return {
            zIndex: i+1
        }
    }
    render(){
        const self = this;
        return (
          <View style={styles.container}>
            {data.map((el, index) => {
                 let combineStyles = StyleSheet.flatten([styles.card, self.zIndex(index)]);
                 return (
                  <View style={combineStyles}>
                      <Card el={el} key={el.id} />
                  </View>
                );
                {/* <View style={styles.child} /> */}
            })}
          </View>
        );
    }
}

export default class App extends Component {
  constructor(){
      super();
      this._listView = null;
      this.scroll = this.scroll.bind(this);
      const ds = new ListView.DataSource({
        getSectionData: (dataBlob, sectionID) => {
          return dataBlob[sectionID];
        },
        getRowData: (dataBlob, sectionID, rowID) => {
          return dataBlob[sectionID + ':' + rowID];
        },
        rowHasChanged: (r1, r2) => r1 !== r2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      });
      this.state = {
        dataSource: ds.cloneWithRowsAndSections(dataBlob, sectionIDs, rowIDs)
      };
  }
  componentDidMount(){
      this._listView.scrollTo(0);
  }
  scroll() {

  }
  render() {
    return (
        <View style={{ flex: 1 }}>
            {/*<ListView
              onScroll={this.scroll}
              ref={(listView) => { this._listView = listView; }}
              stickySectionHeadersEnabled={true}
              dataSource={this.state.dataSource}
              renderRow={(rowData, sectionID, rowID) => <Text>{rowID}</Text>}
              renderSectionHeader={(data, id) => <Card />}
            />*/}
            <ScrollView
              stickyHeaderIndices={[0]}
              ref={(listView) => { this._listView = listView; }}
              onScroll={()=> {this.scroll();}}
            >
              {cards.map(i => {
                return <Card scroll={this.scroll}/>;
              })}
            </ScrollView>
            {/* <View style={styles.container}>
              <Text style={styles.welcome}>
                Welcome to React Native!
              </Text>
              <Text style={styles.instructions}>
                To get started, edit index.ios.js
              </Text>
              <Text style={styles.instructions}>
                Press Cmd+R to reload,{'\n'}
                Cmd+D or shake for dev menu
              </Text>
            </View> */}
      </View>
    );
  }
}
