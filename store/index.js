const imageUrl = "../img/";
const imageName = "rocket.png";
export const imagePath = imageUrl + imageName;
const initialState = [
    {
        id: 1,
        src: imagePath
    }, {
        id: 2,
        src: imagePath
    }, {
      id: 3,
      src: imagePath
    }, {
      id: 4,
      src: imagePath
    }, {
      id: 5,
      src: imagePath
    }
];

export default initialState;
